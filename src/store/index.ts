import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    disciplina: "",
    curso: "",
    profundidade: 0,
    links: [],
    nodes: [],
    enterDown: false,
  },
  mutations: {
    recebeCurso(state, curso) {
      state.curso = curso;
    },
    recebeDisciplina(state, disciplina) {
      state.disciplina = disciplina;
    },
    recebeProfundidade(state, profundidade) {
      state.profundidade = profundidade;
    },
    recebeLinks(state, links) {
      state.links = links;
    },
    recebeNodes(state, nodes) {
      state.nodes = nodes;
    },
    enterDown(state) {
      state.enterDown = true;
    },
    resetEnterDown(state) {
      state.enterDown = false;
    },
  },
  actions: {
    alteraCurso(context, curso) {
      context.commit("recebeCurso", curso);
    },
    alteraDisciplina(context, disciplina) {
      context.commit("recebeDisciplina", disciplina);
    },
    alteraProfundidade(context, profundidade) {
      context.commit("recebeProfundidade", profundidade);
    },
    alteraGrafo(context, grafo) {
      context.commit("recebeLinks", grafo.links);
      context.commit("recebeNodes", grafo.nodes);
    },
    enterDown(context) {
      context.commit("enterDown");
    },
    resetEnterDown(context) {
      context.commit("resetEnterDown");
    },
  },
  getters: {
    curso: (state) => {
      return state.curso;
    },
    disciplina: (state) => {
      return state.disciplina;
    },
    profundidade: (state) => {
      return state.profundidade;
    },
    nodes: (state) => {
      return state.nodes;
    },
    links: (state) => {
      return state.links;
    },
    enterDown: (state) => {
      return state.enterDown;
    },
  },
});
