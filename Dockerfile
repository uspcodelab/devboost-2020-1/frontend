FROM node:13.13-alpine3.10

ENV APP_PATH=/usr/src/app \
    PORT=8080

# mkdir -p /some/path && cd /some/path
WORKDIR ${APP_PATH}

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

EXPOSE ${PORT}

CMD npm run serve
